Yes, you can use the `evtest` command to see all human input device events in real-time on a Wayland system. Here's how you can do it:

1. Open a terminal.
2. Install the `evtest` package if it is not already installed. You can do this by running the following command:
   ```
   sudo apt-get install evtest
   ```
3. Once installed, run the following command to list all available input devices:
   ```
   evtest --query /dev/input/event*
   ```
4. Identify the event number associated with your desired input device (e.g., keyboard, mouse).
5. Run the following command to monitor events from that specific input device:
   ```
   sudo evtest /dev/input/eventX
   ```
   Replace `eventX` with the event number of your chosen input device.

Now, you should be able to see all human input device events in real-time in your terminal window as they occur.
# Fri  7 Jun 18:24:27 CEST 2024 - in the linux commandline in a wayland is there a command i can run in the termal to see all human input device events in real-time?